//
//  TawkTests.swift
//  TawkTests
//
//  Created by Hardik Makwana on 15/03/21.
//

import XCTest
@testable import Tawk

class TawkTests: XCTestCase {

    var viewModel = UsersViewModel()
    var viewDetailModel = UserDetailViewModel()
 
    func testUserModel(){
        let user = UserModel(id: 0, username: "test_login", avatarUrl: "test_avatar_url")
        
        XCTAssertNotNil(user.username)
        XCTAssertNotNil(user.avatarUrl)
    }
    
    func testUserProfileModel(){
     
        let userProfile = UserProfileModel(id: 0, username: "test_login", avatarUrl: "test_avatar_url", followers: 0, following: 0, name: "test_name", company: "test_company", blog: "test_blog")
        
        XCTAssertNotNil(userProfile.username)
        XCTAssertNotNil(userProfile.avatarUrl)
        XCTAssertNotNil(userProfile.name)
        XCTAssertNotNil(userProfile.company)
        XCTAssertNotNil(userProfile.blog)
    
    }
    
    func testGetUsers() {
        self.viewModel.pullDown(
            completion: { users in
                DispatchQueue.main.async {
                 let testUsers = users
                    XCTAssertTrue(testUsers.first?.id != nil)
                }
            },
            otherStatusComplete: { users in
                DispatchQueue.main.async {
                    
                }
            })
    }

    func testUserProfile() {
        
        self.viewModel.pullDown(
            completion: { users in
                DispatchQueue.main.async {
                 
                    let testUser = users.first
                    
                    self.viewModel.getUserProfile(
                        username: testUser!.username,
                        completion: { profile in
                            XCTAssertNotNil(profile)
                        })
                }
            },
            otherStatusComplete: { users in
                DispatchQueue.main.async {
                    
                }
            })
        
        
    }
    func testSaveNotes()  {
        
        self.viewModel.pullDown(
            completion: { users in
                DispatchQueue.main.async {
                    let testUser = users.first
                    DispatchQueue.main.async {
                        testUser!.hasNote = true
                        testUser!.note = "Test Note"
                    }
                    
                    
                    self.viewDetailModel.getNote(
                        userId: testUser!.id,
                        completion: { displayObject in
                            DispatchQueue.main.async {
                                XCTAssertTrue(displayObject.message == "This is a test note")
                                
                            }
                        })
                    
                }
            },
            otherStatusComplete: { users in
                DispatchQueue.main.async {
                    
                }
            })
    }
    
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
