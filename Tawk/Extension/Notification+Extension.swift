//
//  Data+Extension.swift
//  Tawk
//
//  Created by Hardik Makwana on 11/03/21.
//

import UIKit

extension Notification.Name {
    static var networkConnectionChanged: Notification.Name {
        return .init(rawValue: "ConnectionChanged")
    }
}

