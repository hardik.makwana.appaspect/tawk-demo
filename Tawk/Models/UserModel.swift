//
//  UserModel.swift
//  Tawk
//
//  Created by Hardik Makwana on 11/03/21.
//

import UIKit
import CoreData


// MARK: - User Model
class UserModel: ViewModel {
    
    var id: Int32
    var username: String
    var avatarUrl: String
    
    var hasNote: Bool?
    var hasSeen: Bool?
    var note: String?
    // MARK: - User Model Init
   
    init(id: Int32,
         username: String,
         avatarUrl: String,
         hasNote: Bool? = false,
         hasSeen: Bool? = false,
         note: String? = "") {
        
        self.id = id
        self.username = username
        self.avatarUrl = avatarUrl
        
        self.hasNote = hasNote
        self.hasSeen = hasSeen
        self.note = note
    }
}
