//
//  NoteModel.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

// MARK: - Note Model
class NoteModel: ViewModel {
    var message: String
    
    init(message: String) {
        self.message = message
    }
}
