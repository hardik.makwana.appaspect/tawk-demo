//
//  CoreData.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {

}
extension User {
   @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }
    @NSManaged public var avatarUrl: String?
    @NSManaged public var id: Int32
    @NSManaged public var login: String?
    @NSManaged public var seen: Bool
    @NSManaged public var note: Note?
}
extension User : Identifiable {

}
// MARK: - Notes
@objc(Note)
public class Note: NSManagedObject {
}
extension Note {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Note> {
        return NSFetchRequest<Note>(entityName: "Note")
    }
    @NSManaged public var message: String?
    @NSManaged public var user: User?

}
extension Note : Identifiable {

}
