//
//  UserDetailViewModel.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

// MARK: - User Detail Model for the details screen
class UserDetailViewModel: ViewModel {
    
    // MARK: - Get note
    func getNote(
        userId: Int32,
        completion: @escaping (NoteModel) -> ()) {
        
        if let managedUser = DatabaseManager.shared.getUserById(userId: userId),
           let user = managedUser as? User {
            completion(NoteModel(message: user.note?.message ?? ""))
        }
    }
    // MARK: - Save/Update Note
    func userCreateOrUpdateNote(
        userId: Int32,
        message: String,
        completion: @escaping (NoteModel) -> ()) {
        
        DatabaseManager.shared.userCreateOrUpdateNote(
            userId: userId,
            message: message,
            completion: { message in
                completion(NoteModel(message: message ?? ""))
            })
    }
    // MARK: - User seen the Profile Detail screen
    func userSeenProfile(
        userId: Int32,
        completion: @escaping () -> ()) {
        DatabaseManager.shared.seenUser(userId: userId)
        completion()
    }
}

