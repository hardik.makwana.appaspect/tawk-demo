//
//  UserProfileModel.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

// MARK: - User Profile Model
class UserProfileModel: UserModel {
    var followers: Int32
    var following: Int32
    
    var name: String
    var company: String
    var blog: String
    
    // MARK: - User Profile Model init
    init(id: Int32,
         username: String,
         avatarUrl: String,
         followers: Int32,
         following: Int32,
         name: String,
         company: String,
         blog: String) {
        
        self.followers = followers
        self.following = following
        
        self.name = name
        self.company = company
        self.blog = blog
        
        super.init(id: id, username: username, avatarUrl: avatarUrl)
    }
}
