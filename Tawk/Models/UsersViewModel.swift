//
//  UsersViewModel.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit


// MARK: - User View Model to show user list

class UsersViewModel: ViewModel {
    private var users = [UserModel]()
    
    private var pageSize: Int = 5
    private var lastUserId: Int32 = 0
    private var offset: Int = 0
   
    // MARK: - Pull Down
    func pullDown(
        completion: @escaping ([UserModel]) -> (),
        otherStatusComplete: @escaping ([UserModel]) -> ()) {
        
        self.lastUserId = 0
        self.offset = 0
        
        if NetworkManager.shared.isConnectedToNetwork() {
            NetworkManager.shared.getUsers(
                lastUserId: self.lastUserId,
                pageSize: 0,
                completion: { result in
                    switch result {
                    case .success(let resultUsers):
                        // MARK: - convert codable data to display model
                        if let users = resultUsers {
                            self.pageSize = users.count
                            
                            self.users.removeAll()
                            for user in users {
                                DispatchQueue.main.async {
                                    DatabaseManager.shared.userCreateOrUpdate(from: user)
                                }
                                
                                let displayObject = UserModel(id: user.id ?? 0,
                                                              username: user.login ?? "",
                                                              avatarUrl: user.avatarUrl ?? "")
                                self.users.append(displayObject)
                            }
                            // MARK: -
                            // get users note statuses
                            // get users seen statuses
                            // get users note message
                            let userIds = users.map({ $0.id ?? 0})
                            DispatchQueue.main.async {
                                if let managedUsers = DatabaseManager.shared.getUsersByIds(userIds: userIds) as? [User] {
                                    let usersOtherStatus = managedUsers.map(
                                        { user in UserModel(id: user.id,
                                                            username: user.login ?? "",
                                                            avatarUrl: user.avatarUrl ?? "",
                                                            hasNote: user.note != nil,
                                                            hasSeen: user.seen,
                                                            note: user.note?.message)
                                            
                                        })
                                    otherStatusComplete(usersOtherStatus)
                                }
                            }
                            self.offset += self.pageSize
                            if let lastUser = resultUsers?.last,
                               let lastUserId = lastUser.id {
                                self.lastUserId = lastUserId
                            }
                            completion(self.users)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                })
        }else {
            DatabaseManager.shared.getUsers(
                offset: self.offset,
                limit: self.pageSize,
                completion: { result in
                    switch result {
                    case .success(let managedUsers):
                        if let managedUsers = managedUsers as? [User] {
                            let usersOtherStatus = managedUsers.map(
                                { user in UserModel(id: user.id,
                                                    username: user.login ?? "",
                                                    avatarUrl: user.avatarUrl ?? "",
                                                    hasNote: user.note != nil,
                                                    hasSeen: user.seen,
                                                    note: user.note?.message)
                                    
                                })
                            self.offset += self.pageSize
                            
                            // MARK: - determine last user id
                            if let lastUser = usersOtherStatus.last {
                                self.lastUserId = lastUser.id
                            }
                            completion(usersOtherStatus)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                })
            
        }
    }
    // MARK: - Pull Up
    func pullUp(
        completion: @escaping ([UserModel]) -> (),
        otherStatusComplete: @escaping ([UserModel]) -> ()) {
        
        if NetworkManager.shared.isConnectedToNetwork() {
            NetworkManager.shared.getUsers(
                lastUserId: self.lastUserId,
                pageSize: self.pageSize,
                completion: { result in
                    switch result {
                    case .success(let resultUsers):
                        // MARK: - convert codable data to display model
                        if let users = resultUsers {
                            self.users.removeAll()
                            for user in users {
                                DispatchQueue.main.async {
                                    DatabaseManager.shared.userCreateOrUpdate(from: user)
                                }
                                
                                let displayObject = UserModel(id: user.id ?? 0,
                                                              username: user.login ?? "",
                                                              avatarUrl: user.avatarUrl ?? "")
                                self.users.append(displayObject)
                            }
                            // MARK: -
                            // get users note statuses
                            // get users seen statuses
                            // get users note message
                            let userIds = users.map({ $0.id ?? 0})
                            DispatchQueue.main.async {
                                if let managedUsers = DatabaseManager.shared.getUsersByIds(userIds: userIds) as? [User] {
                                    let usersOtherStatus = managedUsers.map(
                                        { user in UserModel(id: user.id,
                                                            username: user.login ?? "",
                                                            avatarUrl: user.avatarUrl ?? "",
                                                            hasNote: user.note != nil,
                                                            hasSeen: user.seen,
                                                            note: user.note?.message)
                                            
                                        })
                                    otherStatusComplete(usersOtherStatus)
                                }
                            }
                            self.offset += self.pageSize
                            // MARK: -
                            // determine last user id
                            if let lastUser = resultUsers?.last,
                               let lastUserId = lastUser.id {
                                self.lastUserId = lastUserId
                            }
                            completion(self.users)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                })
        }else {
            DatabaseManager.shared.getUsers(
                offset: self.offset,
                limit: self.pageSize,
                completion: { result in
                    switch result {
                    case .success(let managedUsers):
                        if let managedUsers = managedUsers as? [User] {
                            let usersOtherStatus = managedUsers.map(
                                { user in UserModel(id: user.id,
                                                    username: user.login ?? "",
                                                    avatarUrl: user.avatarUrl ?? "",
                                                    hasNote: user.note != nil,
                                                    hasSeen: user.seen,
                                                    note: user.note?.message)
                                    
                                })
                            
                            self.offset += self.pageSize
                            // MARK: - determine last user id
                            if let lastUser = usersOtherStatus.last {
                                self.lastUserId = lastUser.id
                            }
                            completion(usersOtherStatus)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                })
        }
    }
    // MARK: - Get Uer Profile for Detail Screen
    func getUserProfile(
        username: String,
        completion: @escaping (UserProfileModel) -> ()) {
        NetworkManager.shared.getUserProfile(
            username: username,
            completion: { result in
                switch result {
                case .success(let profile):
                    if let profile = profile {
                        let displayObject = UserProfileModel(id: profile.id ?? 0,
                                                             username: profile.login ?? "",
                                                             avatarUrl: profile.avatarUrl ?? "",
                                                             followers: profile.followers ?? 0,
                                                             following: profile.following ?? 0,
                                                             name: profile.name ?? "",
                                                             company: profile.company ?? "",
                                                             blog: profile.blog ?? "")
                        completion(displayObject)
                    }
                    
                case .failure(let error):
                    print(error.localizedDescription)
                }
            })
    }
}
