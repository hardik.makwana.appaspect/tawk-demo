//
//  UserCodable.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

// MARK: - User Codable 
struct UserCodable: Codable {
    let id: Int32?
    let login: String?
    let avatarUrl: String?
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case login = "login"
        case avatarUrl = "avatar_url"
    }
    init(from decoder: Decoder) throws {
        let keyedValues = try decoder.container(keyedBy: CodingKeys.self)
        self.login = try keyedValues.decodeIfPresent(String.self, forKey: .login)
        self.id = try keyedValues.decodeIfPresent(Int32.self, forKey: .id)
        self.avatarUrl = try keyedValues.decodeIfPresent(String.self, forKey: .avatarUrl)
    }
}
