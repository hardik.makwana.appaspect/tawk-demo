//
//  NetworkManager.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit


// MARK: - Network Error
enum NetworkRequestError: Error {
    case noInternet
    case httpError(statusCode: Int)
    case serverError(message: String)
    case serializationError(message: String)
    
    private var errorCode: Int {
        switch self {
        case .noInternet: return 100
        case .httpError(_): return 101
        case .serverError(_): return 102
        case .serializationError(_): return 103
        }
    }
    
    var description: String {
        switch self {
        case .noInternet:
            return "There is no internet connection."
        case .httpError(let statusCode):
            return "The call failed with HTTP code \(statusCode)."
        case .serverError(let message):
            return "The server responded with message \"\(message)\"."
        case .serializationError(let message):
            return "JSON Serialization error :: \(message)"
       }
   }
}
// MARK: - Network Request
enum NetworkRequestResult<T> {
    case success(T)
    case failure(NetworkRequestError)
}

// MARK: - Network Monitor
class NetworkManager: NetworkMonitor {
    
    static let shared = NetworkManager()
    // MARK: - Base URL
    static let BASE_URL = "https://api.github.com"
   
    // MARK: - Get USERS
    public func getUsers(
        lastUserId: Int32,
        pageSize: Int,
        completion: @escaping (NetworkRequestResult<[UserCodable]?>) -> ()) {
        
        var urlString = "\(NetworkManager.BASE_URL)/users?since=\(lastUserId))"
        if pageSize > 0 {
            urlString = "\(urlString)&per_page=\(pageSize)"
        }
        guard let url = URL(string: urlString) else {
            return
        }
        
        URLSession.shared.dataTask(
            with: url,
            completionHandler: {
                data, response, error -> Void in
                    if let data = data {
                        do {
                            let jsonDecoder = JSONDecoder()
                            let responseModel = try jsonDecoder.decode([UserCodable].self, from: data)
                           completion(NetworkRequestResult.success(responseModel))
                        }
                        catch let error {
                            completion(NetworkRequestResult.failure(.serializationError(message: error.localizedDescription)))
                        }
                    }
            }).resume()
        
    }
    // MARK: - Get User Profile
    public func getUserProfile(
        username: String,
        completion: @escaping (NetworkRequestResult<UserProfileCodable?>) -> ()) {
        
        guard let url = URL(string: "\(NetworkManager.BASE_URL)/users/\(username)") else {
            return
        }
        URLSession.shared.dataTask(
            with: url,
            completionHandler: {
                data, response, error -> Void in
                    if let data = data {
                        do {
                            let jsonDecoder = JSONDecoder()
                            let responseModel = try jsonDecoder.decode(UserProfileCodable.self, from: data)
                         
                            completion(NetworkRequestResult.success(responseModel))
                        }
                        catch let error {
                            completion(NetworkRequestResult.failure(.serializationError(message: error.localizedDescription)))
                        }
                    }
            }).resume()
    }
}
