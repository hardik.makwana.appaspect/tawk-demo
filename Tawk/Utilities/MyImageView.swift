//
//  MyImageView.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class MyImageView: UIImageView {

    // MARK: - Load Image from url
    func load(url: URL,
              completion: @escaping (UIImage) -> Void) {
        
        let cache = CacheManager.shared.imageCache
        let key = url.absoluteString as NSString
        if let image = cache.object(forKey: key) {
            self.image = image
            completion(image)
            return
        }
        guard NetworkManager.shared.isConnectedToNetwork() else {
            return
        }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = data else { return }
            if let image = UIImage(data: data) {
                cache.setObject(image, forKey: key)
                completion(image)
            }
        }.resume()
        
    }
}
