//
//  CacheManager.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

// MARK: - Cache Manager For the images
class CacheManager {
    static let shared = CacheManager()
    
    public lazy var imageCache = NSCache<NSString, UIImage>()
  
    init() {
        let urlCache = URLCache(
            memoryCapacity: 4 * 1024 * 1024,
            diskCapacity: 300 * 1024 * 1024,
            diskPath: nil)
        URLCache.shared = urlCache
    }
}
