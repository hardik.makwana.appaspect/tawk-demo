//
//  UsersViewProtocol.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

// MARK: - Protocols to update seen and Note Status
protocol UsersViewProtocol {
    func updateSeenStatus(userObject: UserModel)
    func updateNoteStatus(userObject: UserModel)
}
