//
//  InvertedTableViewCell.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

class InvertedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImgView: MyImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var userModel:UserModel!{
        didSet{
            self.nameLabel.attributedText = NSMutableAttributedString().bold(userModel.username)
            if userModel.avatarUrl.count > 0 {
                if let url = URL(string: userModel.avatarUrl) {
                    self.profileImgView.load(
                        url: url,
                        completion: { image in
                           
                            DispatchQueue.main.async {
                                self.profileImgView.image = image
                            }
                        
                    })
                }
            }
            if userModel.hasSeen! {
                self.containerView.backgroundColor = .gray
            }else{
                self.containerView.backgroundColor = .white
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.nameLabel.numberOfLines = 0
        self.selectionStyle = .none
        // Initialization code
    }
}
