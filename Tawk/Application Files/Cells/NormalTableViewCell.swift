//
//  NormalTableViewCell.swift
//  Tawk
//
//  Created by Hardik Makwana on 11/03/21.
//

import UIKit

class NormalTableViewCell: UITableViewCell, ShimmeringViewProtocol {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImgView: MyImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var shimmeringAnimatedItems: [UIView] {
        [
            profileImgView,
            nameLabel
        ]
    }
    
    var userModel:UserModel!{
        didSet{
            self.nameLabel.attributedText = NSMutableAttributedString().bold(userModel.username)
            if userModel.avatarUrl.count > 0 {
                if let url = URL(string: userModel.avatarUrl) {
                    self.profileImgView.load(
                        url: url,
                        completion: { image in
                           
                            DispatchQueue.main.async {
                                self.profileImgView.image = image
                            }
                        
                    })
                }
            }
            if userModel.hasSeen! {
                self.containerView.backgroundColor = .systemGray
            }else{
                self.containerView.backgroundColor = .clear
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.nameLabel.numberOfLines = 0
        self.selectionStyle = .none
        self.layoutSubviews()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
