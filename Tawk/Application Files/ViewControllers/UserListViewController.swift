//
//  UserListViewController.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

class UserListViewController: BaseViewController {
    
    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private lazy var viewModel = UsersViewModel()
    private var users: [UserModel] = [UserModel]()
    private var usersFiltered: [UserModel] = [UserModel]()
    
    private let searchController = UISearchController(searchResultsController: nil)
    private var isSearchBarEmpty: Bool {
        return self.searchController.searchBar.text?.isEmpty ?? true
    }
    private var isSearching: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    
    private var isDataLoading:Bool = true
    
    var statusLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50))
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = UIColor.white
        label.text = "No Internet Connection"
        return label
    }()
    
    var networkView: UIView = {
        let view = UIView(frame: CGRect(x: 0, y: screenHeight - 50, width: screenWidth, height: 50))
        view.backgroundColor = .red
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Users"
        self.userTableView.register(UINib(nibName: "NormalTableViewCell", bundle: nil), forCellReuseIdentifier: "NormalTableViewCell")
        self.userTableView.register(UINib(nibName: "Normal_NoteTableViewCell", bundle: nil), forCellReuseIdentifier: "Normal_NoteTableViewCell")
        self.userTableView.register(UINib(nibName: "InvertedTableViewCell", bundle: nil), forCellReuseIdentifier: "InvertedTableViewCell")
        self.userTableView.register(UINib(nibName: "Inverted_NoteTableViewCell", bundle: nil), forCellReuseIdentifier: "Inverted_NoteTableViewCell")
       
        self.userTableView.allowsMultipleSelection = false
        self.userTableView.dataSource = self
         self.userTableView.delegate = self
        self.userTableView.tableHeaderView = UIView()
        self.userTableView.tableFooterView = UIView()
        self.userTableView.separatorStyle = .none
        
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search by username or note"
        
        self.navigationItem.searchController = searchController
        self.definesPresentationContext = true
        
        self.enableRefreshControl(tableView: self.userTableView)
        
        self.getUsersData()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkConnectionChanged(notification:)),
            name: .networkConnectionChanged,
            object: nil)
        self.activityIndicator.isHidden = true
        
        networkView.addSubview(self.statusLabel)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        showHideNetworkDialog()
    }
    // MARK: - Notification Observers
    @objc func networkConnectionChanged(notification: NSNotification) {
        if let isConnected = notification.object as? Bool {
            if isConnected && !self.isSearching  {
                self.getUsersData()
            }
        }
        self.showHideNetworkDialog()
    }
    func showHideNetworkDialog() {
        if NetworkManager.shared.isConnectedToNetwork() {
            if self.view.contains(networkView) {
                networkView.removeFromSuperview()
            }
        }else{
            self.view.addSubview(networkView)
        }
    }
    
    // MARK: - Get User Data
    private func getUsersData() {
        self.viewModel.pullDown(
            completion: { users in
                DispatchQueue.main.async {
                    self.users = users
                    
                    self.userTableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            },
            otherStatusComplete: { users in
                DispatchQueue.main.async {
                    for user in self.users {
                        for userNoteStatus in users {
                            if user.id == userNoteStatus.id {
                                user.hasNote = userNoteStatus.hasNote
                                user.hasSeen = userNoteStatus.hasSeen
                                user.note = userNoteStatus.note
                                break
                            }
                        }
                    }
                    self.isDataLoading = false
                    self.userTableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            })
    }
    
    private func getNextUsers() {
        self.viewModel.pullUp(
            completion: { users in
                DispatchQueue.main.async {
                    self.users.append(contentsOf: users)
                    
                    self.userTableView.tableFooterView = UIView()
                    self.userTableView.reloadData()
                }
            },
            otherStatusComplete: { users in
                DispatchQueue.main.async {
                    for user in self.users {
                        for userNoteStatus in users {
                            if user.id == userNoteStatus.id {
                                user.hasNote = userNoteStatus.hasNote
                                user.hasSeen = userNoteStatus.hasSeen
                                user.note = userNoteStatus.note
                                break
                            }
                        }
                    }
                    
                    self.userTableView.tableFooterView = UIView()
                    self.userTableView.reloadData()
                }
            })
    }
    
    private func filterContentForSearchText(_ searchText: String) {
        self.usersFiltered = self.users.filter {
            (user: UserModel) -> Bool in
            let hasUsername = user.username.lowercased().contains(searchText.lowercased())
            var hasNote = false
            if let note = user.note {
                hasNote = note.lowercased().contains(searchText.lowercased())
            }
            return hasUsername || hasNote
        }
        
        DispatchQueue.main.async {
            self.userTableView.reloadData()
        }
    }
    
    // MARK: - Handlers
    
    override func refreshData(_ sender: Any) {
        self.getUsersData()
    }
    
}

// MARK: - UItableView
extension UserListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isSearching {
            return self.usersFiltered.count
        }
        return isDataLoading ? 10:self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.isDataLoading {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NormalTableViewCell") as! NormalTableViewCell
            cell.setTemplateWithSubviews(self.isDataLoading, color: .darkGray, animate: true, viewBackgroundColor: .systemBackground)
            return cell
        }else{
            let user: UserModel
            if self.isSearching {
                user = self.usersFiltered[indexPath.row]
            }
            else {
                user = self.users[indexPath.row]
            }
            if (indexPath.row + 1) % 4 == 0 {
                if !user.hasNote! {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "InvertedTableViewCell") as! InvertedTableViewCell
                    cell.userModel = user
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Inverted_NoteTableViewCell") as! Inverted_NoteTableViewCell
                    cell.userModel = user
                    return cell
                }
            }
            else {
                if !user.hasNote! {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "NormalTableViewCell") as! NormalTableViewCell
                    cell.userModel = user
                    cell.setTemplateWithSubviews(self.isDataLoading, color: .darkGray, animate: true, viewBackgroundColor: .systemBackground)
                   
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Normal_NoteTableViewCell") as! Normal_NoteTableViewCell
                    cell.userModel = user
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isDataLoading {
            return
        }
        let user: UserModel
        if self.isSearching {
            user = self.usersFiltered[indexPath.row]
        }
        else {
            user = self.users[indexPath.row]
        }
        self.activityIndicator.isHidden = false
        self.view.isUserInteractionEnabled = false
        self.viewModel.getUserProfile(
            username: user.username,
            completion: { profile in
                DispatchQueue.main.async {
                    self.activityIndicator.isHidden = true
                    self.view.isUserInteractionEnabled = true
                    let profileVC = ProfileDetailViewController(nibName: "ProfileDetailViewController", bundle: nil)
                    profileVC.userModel = user
                    profileVC.userProfileModel = profile
                    profileVC.delegate = self
                    self.navigationController?.pushViewController(profileVC, animated: true)
                    
                }
                
            })
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // pull-up
        if (self.userTableView.contentOffset.y + self.userTableView.frame.size.height) >= self.userTableView.contentSize.height {
             DispatchQueue.main.async {
                let spinner = UIActivityIndicatorView(style: .large)
                spinner.color = UIColor.label
                spinner.hidesWhenStopped = true
                self.userTableView.tableFooterView = spinner
                spinner.startAnimating()
                self.getNextUsers()
             }
        }
    }
}
// MARK: - Search Delegate
extension UserListViewController: UISearchBarDelegate {
    
}
extension UserListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        // If the search bar contains text, filter our data with the string
        if let searchText = searchController.searchBar.text {
            self.filterContentForSearchText(searchText)
        }
    }
}
// MARK: - User Protocols
extension UserListViewController: UsersViewProtocol {
    func updateNoteStatus(userObject displayObject: UserModel) {
        if let userDO = self.users.first(where: { $0.id == displayObject.id }) {
            DispatchQueue.main.async {
                userDO.hasNote = true
                userDO.note = displayObject.note
                self.userTableView.reloadData()
            }
        }
    }
    func updateSeenStatus(userObject displayObject: UserModel) {
        if let displayObject = self.users.first(where: { $0.id == displayObject.id }) {
            DispatchQueue.main.async {
                displayObject.hasSeen = true
                self.userTableView.reloadData()
            }
        }
    }
}
