//
//  BaseViewController.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit


// Screen width.
public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

// Screen height.
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}


class BaseViewController: UIViewController {

    let refreshControl = UIRefreshControl()
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func enableRefreshControl(tableView: UITableView) {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        }
        else {
            tableView.addSubview(refreshControl)
        }
        self.refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        let color = UIColor.black
        let attributes = [NSAttributedString.Key.foregroundColor: color]
        self.refreshControl.tintColor = color
        self.refreshControl.attributedTitle = NSAttributedString(string: "Loading...", attributes: attributes)
    }

    // MARK: - Handlers
    
    @objc func refreshData(_ sender: Any) {
        DispatchQueue.main.asyncAfter(
            deadline: .now() + 3,
            execute: {
                self.refreshControl.endRefreshing()
        })
    }
    
}

