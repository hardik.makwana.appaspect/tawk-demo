//
//  ProfileDetailViewController.swift
//  Tawk
//
//  Created by Hardik Makwana on 12/03/21.
//

import UIKit

class ProfileDetailViewController: BaseViewController {

    @IBOutlet weak var profileImgView: MyImageView!
    @IBOutlet weak var followersLabel: UILabel!
    
    @IBOutlet weak var followingLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var blogLabel: UILabel!
    
    @IBOutlet weak var noteTextView: UITextView!
    
    private lazy var viewModel = UserDetailViewModel()
    var userModel:UserModel?
    var userProfileModel: UserProfileModel?
    var delegate: UsersViewProtocol?
    
    // MARK: -  View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = userProfileModel?.username ?? ""
        self.nameLabel.attributedText = NSMutableAttributedString().bold("Name: ").normal("\(userProfileModel?.username ?? "")")
        self.companyLabel.attributedText = NSMutableAttributedString().bold("Company: ").normal("\(userProfileModel?.company ?? "")")
        self.blogLabel.attributedText = NSMutableAttributedString().bold("Blog: ").normal("\(userProfileModel?.blog ?? "")")
        
        self.followersLabel.attributedText = NSMutableAttributedString().bold("Followers: ").normal("\(userProfileModel?.followers ?? 0)")
        self.followingLabel.attributedText = NSMutableAttributedString().bold("Following: ").normal("\(userProfileModel?.following ?? 0)")
        
        if userProfileModel!.avatarUrl.count > 0 {
            if let url = URL(string: userProfileModel!.avatarUrl) {
                self.profileImgView.load(
                    url: url,
                    completion: { image in
                        DispatchQueue.main.async {
                            self.profileImgView.contentMode = .scaleAspectFill
                            self.profileImgView.image = image
                        }
                    })
            }
        }
        self.getNote()
        self.seenUser()
        
    }
    // MARK: - Get Note
    private func getNote() {
        if let userDO = self.userProfileModel {
            self.viewModel.getNote(
                userId: userDO.id,
                completion: { displayObject in
                    DispatchQueue.main.async {
                        self.noteTextView.text = displayObject.message
                        
                    }
                })
        }
    }
    // MARK: -  See User
    private func seenUser() {
        if let userDO = self.userProfileModel {
            self.viewModel.userSeenProfile(
                userId: userDO.id,
                completion: {
                    self.delegate?.updateSeenStatus(userObject: userDO)
                })
        }
    }
    
    // MARK: - Save Button Click
    @IBAction func saveButtonClick(_ sender: Any) {
    
        if self.noteTextView.text.count > 0 {
            
            
            if let displayObject = self.userProfileModel {
                self.viewModel.userCreateOrUpdateNote(
                    userId: displayObject.id,
                    message: self.noteTextView.text,
                    completion: { noteDisplayObject in
                        let savedNote = noteDisplayObject.message
                        displayObject.note = savedNote
                        self.delegate?.updateNoteStatus(userObject: displayObject)
                        
                        self.navigationController?.popViewController(animated: true)
                    })
            }
        }
    }
}
